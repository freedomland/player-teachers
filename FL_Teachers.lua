mpWidgets = require("mpWidgets")
classes = require("classes")
CommonFunctions = require("CommonFunctions")
tableHelper = require("tableHelper")
require("color")
require("translate")

Methods = {}

function PriceDialog(pid, targetPid, skill)
	local menuId = config.customMenuIds.FLTeachPrice
	
	mpWidgets.New(menuId, tr("Good. Now name your price."), tr("Price"))
	mpWidgets.OnOkClicked(menuId, function(str) CheckPrice(pid, targetPid, skill, str) end)
	mpWidgets.Show(targetPid, menuId, 3)
end

function WarningDialog(pid, targetPid, skill)
	local menuId = config.customMenuIds.FLTeachWarn
	
	tes3mp.CustomMessageBox(pid, -1, color.White .. tr("Waiting for ") .. Players[targetPid].chat.accountName .. tr(" to comfirm. Please wait..."), "Ok")
	
	mpWidgets.New(menuId, Players[pid].chat.accountName .. tr(" wants to study from you ") .. skill .. " do you agree?")
	mpWidgets.AddButton(menuId, 0, tr("Yes"), function() PriceDialog(pid, targetPid, skill) end)
	mpWidgets.AddButton(menuId, 1, tr("No"), function() tes3mp.CustomMessageBox(pid, -1, Players[targetPid].chat.accountName .. tr(" dont't want to train you."), "Ok") end)
	mpWidgets.Show(targetPid, menuId, 1)
end

function CheckLevel(pid, price)
	local level = Players[pid].data.stats.level
	
	if level <= 25 then
		return price >= 50 and price <= 100
	elseif level <= 50 then
		return price >= 100 and price <= 200
	elseif level <= 75 then
		return price >= 200 and price <= 500
	elseif level <= 90 then
		return price >= 500 and price <= 1000
	end
	
	return false
end

function CanAfford(pid)
	local gold = CommonFunctions.GetGold(pid)
	local level = Players[pid].data.stats.level
	
	if level <= 25 then
		return gold >= 50
	elseif level <= 50 then
		return gold >= 100
	elseif level <= 75 then
		return gold >= 200
	elseif level <= 90 then
		return gold >= 500
	end
	
	return false
end

function CheckPrice(pid, targetPid, skill, str)
	local price = tonumber(str)

	if price == nil then
		tes3mp.MessageBox(targetPid, -1, "#FF0000" .. tr("Price must be numeric value."))
		PriceDialog(pid, targetPid, skill)
		return
	end

	if price == -1 then
		tes3mp.CustomMessageBox(pid, -1, Players[targetPid].chat.accountName .. tr(" dont't want to train you."), "Ok")
		return
	end

	if price > CommonFunctions.GetGold(pid) then
		tes3mp.CustomMessageBox(targetPid, -1, Players[pid].chat.accountName .. tr(" can't afford this study. Either reduce price or input -1 to cancel."), "Ok")
		PriceDialog(pid, targetPid, skill)
		return
	end
	
	if CheckLevel(pid, price) == false then
		tes3mp.MessageBox(targetPid, -1, "#FF0000" .. tr("Invalid price! Please follow instructions."))
		PriceDialog(pid, targetPid, skill)
		return
	end
	
	local menuId = config.customMenuIds.FLTeachConfirm
	
	mpWidgets.New(menuId, Players[targetPid].chat.accountName .. tr(" want you to pay for tuition ") .. price .. tr(" do you agree?"))
	mpWidgets.AddButton(menuId, 0, tr("Yes"), function() Teach(pid, targetPid, skill, price) end)
	mpWidgets.AddButton(menuId, 1, tr("No"), function() tes3mp.CustomMessageBox(targetPid, -1, Players[pid].chat.accountName .. tr(" dont agree with your price."), "Ok") end)
	mpWidgets.Show(pid, menuId, 1)
end

function Teach(pid, targetPid, skill, price)
	local skillId = tes3mp.GetSkillId(skill)
	local progress = Players[pid].data.skillProgress[skill]
	local skillValue =  Players[pid].data.skills[skill]
	
	CommonFunctions.RemoveGold(pid, price)
	
	if math.floor(progress) + 4 >= 20 then
		Players[pid].data.skillProgress[skill] = math.floor(progress) + 4 - 20
		Players[pid].data.skills[skill] = skillValue + 1
		tes3mp.SetSkillProgress(pid, skillId, math.floor(progress) + 4 - 20)
		tes3mp.SetSkillBase(pid, skillId, skillValue + 1)
		tes3mp.MessageBox(pid, -1, tr("Your ") .. skill .. " increased to " .. skillValue + 1)
		tes3mp.PlaySpeech(pid, "Fx/Inter/levelUP.wav")
		
		local class = Players[pid].data.character.class
		if tableHelper.containsValue(classes[class].majorSkills, skill, false) == true or 
				tableHelper.containsValue(classes[class].minorSkills, skill, false) == true then
			Players[pid].data.stats.levelProgress = Players[pid].data.stats.levelProgress + 1
			tes3mp.SetLevelProgress(pid, Players[pid].data.stats.levelProgress)
			tes3mp.SendLevel(pid)
		end
	else
		Players[pid].data.skillProgress[skill] = progress + 4
		tes3mp.SetSkillProgress(pid, skillId, progress + 4)
		local visual = (math.floor(progress) + 4) * 100 / 20
		tes3mp.MessageBox(pid, -1, tr("Skill progress ") .. skill .. " " .. visual .. "/100")
	end
	
	tes3mp.SendSkills(pid)
end

function GetMajorSkills(targetPid)
	local targetData = Players[targetPid].data
	local targetClass = targetData.character.class
	
	if targetClass == "custom" then
		return targetData.customClass.majorSkills
	end

	local targetSkills = targetData.skills
	
	local skills = {}
	
	for i, c in pairs(classes[targetClass].majorSkills) do
		table.insert(skills, c)
	end
	
	return skills
end

function GetFullSkills(targetPid)
	local skills = {}

	for i, c in pairs(GetMajorSkills(targetPid)) do
		if Players[targetPid].data.skills[c] == 100 then
			table.insert(skills, c)
		end
	end
	
	return skills
end

function SetSkills(targetPid, tab)
	local skills = GetMajorSkills(targetPid)
	Players[targetPid].data.customVariables.tskills = {}

	for i, t in pairs(tab) do
		table.insert(Players[targetPid].data.customVariables.tskills, skills[i])
	end
end

function Methods.CheckSkills(targetPid)
	local skills = GetMajorSkills(targetPid)

	local menuId = config.customMenuIds.FLTeachCheck
	
	mpWidgets.New(menuId, tr("Select 3 skills you want to teach. You can choose only major skills. Once you click done skills can't be changed."))
	mpWidgets.SetLimits(menuId, 3, 3)
	
	for i, c in pairs(skills) do
		mpWidgets.AddCheckBox(targetPid, menuId, i-1, tr(c))
	end
	
	mpWidgets.OnDoneClicked(targetPid, menuId, function(tab) SetSkills(targetPid, tab) end)
	mpWidgets.Show(targetPid, menuId, 4)
end

function Methods.ShowMenu(pid, targetPid)
	if GetFullSkills(targetPid) == nil then
		tes3mp.CustomMessageBox(pid, -1, Players[targetPid].chat.accountName .. color.White .. tr(" cant teach you, because he or she doesnt have skills up to 100."), "Ok")
		return
	end
	
	local skills = Players[targetPid].data.customVariables.tskills

	if skills == nil then
		tes3mp.CustomMessageBox(targetPid, -1, Players[pid].chat.accountName .. color.White .. tr(" wants to study from you, but you doesn't selected 3 skills whom you want to teach. Enter ") .. color.Yellow .. "/mm" .. color.White .. tr(" and click Teach to select skills."), "Ok")
		tes3mp.CustomMessageBox(pid, -1, Players[targetPid].chat.accountName .. color.White .. tr(" cant teach you, because he or she doesnt selected skills he or she wants to teach."), "Ok")
		return
	end
	
	if CanAfford(pid) == false then
		tes3mp.CustomMessageBox(pid, -1, tr("You cant afford this."), "Ok")
		return
	end

	local menuId = config.customMenuIds.FLTeach
	local num = 0
	
	mpWidgets.New(menuId, tr("Study"))
	
	for i, c in pairs(skills) do
		if Players[targetPid].data.skills[c] == 100 then
			mpWidgets.AddButton(menuId, num, c, function() WarningDialog(pid, targetPid, c) end)
			num = num + 1
		end
	end
	
	mpWidgets.AddButton(menuId, num, tr("Exit"), nil)
	mpWidgets.Show(pid, menuId, 1)
end

return Methods
